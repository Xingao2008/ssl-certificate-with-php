 <?php
	private function get_content_via_curl($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, '1');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, '1');
        curl_setopt($ch, CURLOPT_SSLCERTTYPE,"PEM");
        curl_setopt($ch, CURLOPT_CAINFO, getcwd().'\\certificate\\cacert.pem');
//        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, "Mustang123");

        curl_setopt($ch, CURLOPT_SSLCERT, getcwd().'\\certificate\\CIMS-Mambourin-PEM.crt');
        curl_setopt($ch, CURLOPT_SSLKEYTYPE, 'PEM');
        curl_setopt($ch, CURLOPT_SSLKEY, getcwd().'\\certificate\\private-CIMS-Mambourin.key');
        $response = curl_exec($ch);
        if( $response === false)
        {
            $info = curl_getinfo($ch);
            echo 'Took ' . $info['total_time'] . ' seconds to send a request to ' . $info['url'];
            echo 'Curl error: ' . curl_error($ch);
        }else{
            echo "success";
        }

        curl_close($ch);
        return $response;
    }